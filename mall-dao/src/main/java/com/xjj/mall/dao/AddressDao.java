/****************************************************
 * Description: DAO for t_mall_address
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/
package com.xjj.mall.dao;

import com.xjj.mall.entity.AddressEntity;
import com.xjj.framework.dao.XjjDAO;

public interface AddressDao  extends XjjDAO<AddressEntity> {
	
}

